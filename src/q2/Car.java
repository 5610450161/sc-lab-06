package q2;

public abstract class Car {
	private String name;
	
	public Car(String name){
		this.name = name;
	}
	
	public abstract String type();
	
	public String toString(){
		return this.name;
	}
	
}
