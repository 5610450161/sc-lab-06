package lab06_1;

import java.util.Scanner;

public class Test {

			public static void main(String[] args){
				int T;
				
				Scanner input = new Scanner(System.in);
				T = input.nextInt();
				while (T > 0){
					long n = input.nextLong();
					int s = (int)Math.sqrt(n);
					boolean x = true;
					if(n == 1){
						x = false ;
					}
					else if (n != 1) {
						for(int a=2;a<=s;a++){
							if(n%a==0 ){
								x = false ;
								break ;
							}
							else{
								x = true ;
							}
						}
					}
					System.out.println(x);
					T--;
				}
			}
	
}
